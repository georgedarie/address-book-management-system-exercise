**Requirements:**

* An AddressBook contains several Contacts
* It offers the possibility to add, remove contacts and search contacts based on name
* A Contact has one or more ContactData items
* AddressBooks can be persisted using a StorageImplementation

**Bonus points:**

* Contact duplicates by name are note permitted
* Use file storage with java serialization