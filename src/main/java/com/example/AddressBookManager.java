package com.example;

import com.example.addressBook.AddressBook;
import com.example.model.Contact;
import com.example.model.contactData.information.AddressInformation;
import com.example.storage.Storage;

public class AddressBookManager {
    private AddressBook addressBook;
    private Storage storage;

    public AddressBookManager(Storage storage) {
        this.storage = storage;
        this.addressBook = storage.load();
    }

    public void add(Contact contact) throws Exception {
        this.addressBook.add(contact);
        this.storage.store(this.addressBook);
    }

    public void delete(String name) {
        this.addressBook.delete(name);
        this.storage.store(this.addressBook);
    }

    public Contact find(String name) {
        return this.addressBook.find(name);
    }
}
