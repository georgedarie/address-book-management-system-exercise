package com.example.addressBook;

import com.example.model.Contact;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class AddressBook implements Serializable {
    private List<Contact> contacts;

    public AddressBook() {
        this.contacts = new ArrayList<>();
    }

    public void add(Contact contact) throws Exception {
        if (this.find(contact.getName()) == null) {
            this.contacts.add(contact);
        } else {
            throw new Exception("Contacts with duplicated names are not allowed!");
        }
    }

    public void delete(String name) {
        this.contacts.removeIf(contact -> contact.getName().equals(name));
    }

    public Contact find(String name) {
        return this.contacts
                .stream()
                .filter(contact -> contact.getName().equals(name))
                .findFirst()
                .orElse(null);
    }
}
