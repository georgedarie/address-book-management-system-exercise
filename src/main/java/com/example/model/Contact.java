package com.example.model;

import com.example.model.contactData.ContactData;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Contact implements Serializable {
    private String name;
    private List<ContactData> data;

    public Contact() {
        this.data = new ArrayList<>();
    }

    public Contact(String name) {
        this.data = new ArrayList<>();
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    public List<ContactData> getData() {
        return this.data;
    }

    public void addData(ContactData contactData) {
        this.data.add(contactData);
    }
}
