package com.example.model.contactData;

import java.io.Serializable;

public interface ContactData<T> extends Serializable {
    void setValue(T value);
    T getValue();
}
