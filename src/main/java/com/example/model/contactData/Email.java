package com.example.model.contactData;

import com.example.model.contactData.information.EmailInformation;

import java.io.Serializable;

public class Email implements ContactData<EmailInformation> {

    private EmailInformation value;

    @Override
    public void setValue(EmailInformation value) {
        this.value = value;
    }

    @Override
    public EmailInformation getValue() {
        return this.value;
    }
}
