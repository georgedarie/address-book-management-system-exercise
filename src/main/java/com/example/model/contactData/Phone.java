package com.example.model.contactData;

import com.example.model.contactData.information.PhoneInformation;

import java.io.Serializable;

public class Phone implements ContactData<PhoneInformation> {

    private PhoneInformation value;

    @Override
    public void setValue(PhoneInformation value) {
        this.value = value;
    }

    @Override
    public PhoneInformation getValue() {
        return this.value;
    }
}
