package com.example.model.contactData;

import com.example.model.contactData.information.AddressInformation;

public class Address implements ContactData<AddressInformation> {
    private AddressInformation value;

    @Override
    public void setValue(AddressInformation value) {
        this.value = value;
    }

    @Override
    public AddressInformation getValue() {
        return this.value;
    }
}
