package com.example.model.contactData.information;

import java.io.Serializable;

public class PhoneInformation implements Serializable {
    private String additionalInformation;
    private String phoneNumber;

    private PhoneInformation() {
    }

    public String getAdditionalInformation() {
        return this.additionalInformation;
    }

    public String getPhoneNumber() {
        return this.phoneNumber;
    }

    public static Builder build() {
        return new Builder();
    }

    public static class Builder {
        private final PhoneInformation phoneInformation;

        private Builder() {
            this.phoneInformation = new PhoneInformation();
        }

        public Builder additionalInformation(String additionalInformation) {
            this.phoneInformation.additionalInformation = additionalInformation;
            return this;
        }

        public Builder phoneNumber(String phoneNumber) {
            this.phoneInformation.phoneNumber = phoneNumber;
            return this;
        }

        public PhoneInformation get() {
            return this.phoneInformation;
        }
    }
}
