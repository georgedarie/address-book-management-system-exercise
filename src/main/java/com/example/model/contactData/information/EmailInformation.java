package com.example.model.contactData.information;

import java.io.Serializable;

public class EmailInformation implements Serializable {
    private String additionalInformation;
    private String emailAddress;

    private EmailInformation() {

    }

    public String getAdditionalInformation() {
        return this.additionalInformation;
    }

    public String getEmailAddress() {
        return this.emailAddress;
    }

    public static Builder build() {
        return new Builder();
    }

    public static class Builder {
        private final EmailInformation emailInformation;

        private Builder() {
            this.emailInformation = new EmailInformation();
        }

        public Builder additionalInformation(String additionalInformation) {
            this.emailInformation.additionalInformation = additionalInformation;
            return this;
        }

        public Builder emailAddress(String emailAddress) {
            this.emailInformation.emailAddress = emailAddress;
            return this;
        }

        public EmailInformation get() {
            return this.emailInformation;
        }
    }
}
