package com.example.model.contactData.information;

import java.io.Serializable;

public class AddressInformation implements Serializable {
    private String additionalInformation;
    private String country;
    private String region;
    private String city;
    private String street;
    private String building;
    private String apartment;

    private AddressInformation() {
    }

    public String getAdditionalInformation() {
        return this.additionalInformation;
    }

    public String getCountry() {
        return this.country;
    }

    public String getRegion() {
        return this.region;
    }

    public String getCity() {
        return this.city;
    }

    public String getStreet() {
        return this.street;
    }

    public String getBuilding() {
        return this.building;
    }

    public String getApartment() {
        return this.apartment;
    }

    public static Builder build() {
        return new Builder();
    }

    public static class Builder {
        private final AddressInformation addressInformation;

        private Builder() {
            this.addressInformation = new AddressInformation();
        }

        public Builder additionalInformation(String additionalInformation) {
            this.addressInformation.additionalInformation = additionalInformation;
            return this;
        }

        public Builder country(String country) {
            this.addressInformation.country = country;
            return this;
        }

        public Builder region(String region) {
            this.addressInformation.region = region;
            return this;
        }

        public Builder city(String city) {
            this.addressInformation.city = city;
            return this;
        }

        public Builder street(String street) {
            this.addressInformation.street = street;
            return this;
        }

        public Builder building(String building) {
            this.addressInformation.building = building;
            return this;
        }

        public Builder apartment(String apartment) {
            this.addressInformation.apartment = apartment;
            return this;
        }

        public AddressInformation get() {
            return this.addressInformation;
        }
    }
}
