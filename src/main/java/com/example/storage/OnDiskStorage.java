package com.example.storage;

import com.example.addressBook.AddressBook;

import java.io.*;

public class OnDiskStorage implements Storage {
    private String filePath;

    public OnDiskStorage(String filePath) {
        this.filePath = filePath;
    }

    @Override
    public void store(AddressBook addressBook) {
        try {
            writeAddressBookToFile(addressBook);
        } catch (IOException e) {
            System.err.println(
                    "Could not store the modifications to the file containing the address book!\n" +
                    "More information:\n"
            );
            e.printStackTrace();
        }
    }

    @Override
    public AddressBook load() {
        try {
            return getAddressBookFromFile();
        } catch (IOException e) {
            System.err.println("Could not access file which contains the address book!\nMore information:\n");
            e.printStackTrace();
            return null;
        } catch (ClassNotFoundException e) {
            System.err.println("Could not class matching read object!\nMore information:");
            e.printStackTrace();
            return null;
        }
    }

    private void writeAddressBookToFile(AddressBook addressBook) throws IOException {
        try (FileOutputStream fileOutputStream = new FileOutputStream(this.filePath);
                ObjectOutputStream outputStream = new ObjectOutputStream(fileOutputStream)) {
            outputStream.writeObject(addressBook);
        }
    }

    private AddressBook getAddressBookFromFile() throws IOException, ClassNotFoundException {
        try (FileInputStream fileInputStream = new FileInputStream(this.filePath);
             ObjectInputStream inputStream = new ObjectInputStream(fileInputStream)) {
            return (AddressBook) inputStream.readObject();
        }
    }
}
