package com.example.storage;

import com.example.addressBook.AddressBook;

public interface Storage {
    void store(AddressBook addressBook);
    AddressBook load();
}
