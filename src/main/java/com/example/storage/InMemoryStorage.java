package com.example.storage;

import com.example.addressBook.AddressBook;

public class InMemoryStorage implements Storage {
    private AddressBook addressBook;

    @Override
    public void store(AddressBook addressBook) {
        this.addressBook = addressBook;
    }

    @Override
    public AddressBook load() {
        return this.addressBook;
    }
}
