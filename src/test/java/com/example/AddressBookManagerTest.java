package com.example;

import com.example.addressBook.AddressBook;
import com.example.model.Contact;
import com.example.model.contactData.Address;
import com.example.model.contactData.Email;
import com.example.model.contactData.Phone;
import com.example.model.contactData.information.AddressInformation;
import com.example.model.contactData.information.EmailInformation;
import com.example.model.contactData.information.PhoneInformation;
import com.example.storage.OnDiskStorage;
import org.junit.Test;

import java.net.URL;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.junit.Assert.*;

public class AddressBookManagerTest {
    private Phone createContactPhone(Integer index) {
        Phone phone = new Phone();
        phone.setValue(
                PhoneInformation
                        .build()
                        .additionalInformation("This is the phone for Contact Name " + index)
                        .phoneNumber("0123456789_" + index)
                        .get()
        );
        return phone;
    }

    private Address createContactAddress(Integer index) {
        Address address = new Address();
        address.setValue(
                AddressInformation
                        .build()
                        .additionalInformation("This is the address for Contact Name " + index)
                        .country("Country " + index)
                        .region("Region " + index)
                        .city("City " + index)
                        .street("Street " + index)
                        .building("Building " + index)
                        .apartment("Apartment " + index)
                        .get()
        );
        return address;
    }

    private Email createContactEmail(Integer index) {
        Email email = new Email();
        email.setValue(
                EmailInformation
                        .build()
                        .additionalInformation("This is the email for Contact Name " + index)
                        .emailAddress("Email address " + index)
                        .get()
        );
        return email;
    }

    private Contact createContact(Integer index) {
        Contact contact = new Contact("Contact Name " + index);
        contact.addData(createContactPhone(index));
        contact.addData(createContactAddress(index));
        contact.addData(createContactEmail(index));
        return contact;
    }

    private List<Contact> createContacts(Integer numberOfContactsToCreate) {
        return IntStream
                .range(0, numberOfContactsToCreate)
                .boxed()
                .map(this::createContact)
                .collect(Collectors.toList());
    }

    private AddressBook createAddressBook(Integer numberOfContactsToContain) {
        AddressBook addressBook = new AddressBook();
        List<Contact> contacts = createContacts(numberOfContactsToContain);
        for (Contact contact : contacts) {
            try {
                addressBook.add(contact);
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }
        return addressBook;
    }

    private void generateAndStoreAddressBookToFile(String filePath, Integer numberOfContactsToContain) {
        OnDiskStorage onDiskStorage = new OnDiskStorage(filePath);
        onDiskStorage.store(createAddressBook(numberOfContactsToContain));
    }

    private AddressBookManager getAddressBookManager() throws Exception {
        Optional<URL> testAddressBookFileURL = Optional
                .ofNullable(
                        Thread
                                .currentThread()
                                .getContextClassLoader()
                                .getResource("test-address-book")
                );
        String testAddressBookFileLocation = testAddressBookFileURL
                .map(URL::getFile)
                .orElse(null);
        if (testAddressBookFileLocation == null) {
            throw new Exception("Could not find resource with name test-address-book.");
        }
        OnDiskStorage onDiskStorage = new OnDiskStorage(testAddressBookFileLocation);
        return new AddressBookManager(onDiskStorage);
    }

    @Test
    public void testAddFindAndDelete() {
        AddressBookManager addressBookManager = null;
        try {
            addressBookManager = getAddressBookManager();
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.getMessage());
            fail();
        }
        String testContactName = "Test Contact Name";
        Contact testContact = new Contact(testContactName);
        try {
            addressBookManager.add(testContact);
            Contact actualContact = addressBookManager.find(testContactName);
            assertNotNull(actualContact);
            assertEquals(actualContact.getName(), testContact.getName());
        } catch (Exception e) {
            e.printStackTrace();
            fail();
        }
        try {
            addressBookManager.add(testContact);
            fail();
        } catch (Exception e) {
            assertEquals("Contacts with duplicated names are not allowed!", e.getMessage());
        }
        addressBookManager.delete(testContactName);
        assertNull(addressBookManager.find(testContactName));
    }
}
